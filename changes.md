[unreleased]

#### v1.2.6 (2017-03-06)
**Implemented enhancements:**
- Image size config for block innovent-team-2

#### v1.2.5 (2017-03-05)
**Fixed bugs:**
- Load bootstrap in this pluin from CDN

**Implemented enhancements:**
- new block for parallax background images

#### v1.2.3 (2017-02-19)
**Implemented enhancements:**
- change type textarea to editor in innovent-team-2

#### v1.2.2 (2017-02-12)
**Implemented enhancements:**
- more settings for innovent-header-1 and innovent-team-2

**Fixed bugs:**
- all settings have an effect (innovent-header-1)

#### v1.2.1 (2017-02-12)
**Implemented enhancements:**
- modified block "contact form"

#### v1.2.0 (2017-02-12)
**Implemented enhancements:**
- block contact form added

#### v1.1.1 (2017-02-03)
**Implemented enhancements:**
- Group timelines added

**Other Changes:**
- New Plugin description

#### v1.1.0 (2017-01-30)
**Implemented enhancements:**
- Adding block team with description text

#### v1.0.1 (2017-01-30)
**Fixed bugs:**
- display problems horizontal timeline
- display problems with responsive zigzag timeline

#### v1.0.0 (2017-01-28)
Plugin release with the first blocks.