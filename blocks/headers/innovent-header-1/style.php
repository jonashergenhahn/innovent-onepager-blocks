#<?php echo $id ?>{
	<?php if($styles['bg_image']):?>
	background-image: url(<?php echo $styles['bg_image']?>);
	background-repeat: <?php echo $styles['bg_repeat']?>;
	background-size : cover;
	<?php endif;?>
	<?php if($styles['bg_parallax']):?>
	background-attachment : fixed;
	<?php endif;?>
	background-color : <?php echo $styles['bg_color'] ?>;
	color : <?php echo $styles['text_color']?>;
	height: <?php echo $settings['section_height'];?><?php echo $settings['section_height']==='auto'? '' : 'px';?>;
}
#<?php echo $id; ?> .navbar{
	background-color : <?php echo $styles['nav_bg_static']?>
}
#<?php echo $id; ?> .navbar.affix{
	background-color : <?php echo $styles['nav_bg']?>
}
#<?php echo $id ?> .navbar-nav > li > a{
	color : <?php echo $styles['link_color']; ?>;
}
#<?php echo $id ?> .navbar-toggle .icon-bar{
	background-color : <?php echo $styles['link_color']; ?>;
}
#<?php echo $id ?> .navbar-nav > li:hover > a{
	color : <?php echo $styles['link_hover_color']; ?>;
}
#<?php echo $id ?> .navbar-nav > li.active > a {
	color: #fff;
	background-color: <?php echo $styles['link_hover_color']; ?>;
}
#<?php echo $id ?> .section-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
}
#<?php echo $id ?> .section-desc {
	font-size : <?php echo $settings['text_size']?>rem;
}
#<?php echo $id ?> .btn{
	background: transparent;
	border: 3px solid <?php echo $styles['button_border_color']?>;
	color : <?php echo $styles['button_text_color']?>;
}
#<?php echo $id ?> .btn:hover{
	background: <?php echo $styles['button_border_color']?>;
	color : #fff;
}
/*
	Collapse Navbar
*/
@media (max-width: <?php echo $settings['nav_collapse']?>px) {
  .navbar-header {float: none;}
  .navbar-left,.navbar-right {float: none !important;}
  .navbar-toggle {display: block;}
  .navbar-collapse {border-top: 1px solid transparent;box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);}
  .navbar-fixed-top {top: 0;border-width: 0 0 1px;}
  .navbar-collapse.collapse {display: none!important;}
  .navbar-nav {float: none!important;margin-top: 7.5px;  }
  .navbar-nav>li {float: none;}
  .navbar-nav>li>a {padding-top: 10px;padding-bottom: 10px;}
  .collapse.in{display:block !important;}
}