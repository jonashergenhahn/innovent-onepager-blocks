
<?php
	// Media Grid
	$image_cols = $settings['media_grid'];
	switch($settings['media_alignment']) {
		case 'left':
		case 'right':
			$content_cols = 12 - $image_cols;
			break;
		default:
			$content_cols = 12; // Default 12 grid
	}

	// Animation
	$animation_media = ($settings['animation_media']) ? $settings['animation_media'] : '';
	$animation_content = ($settings['animation_content']) ? $settings['animation_content'] : '';
	// Padding alignment map
	$padding_class = ( $settings['media_alignment'] == 'left' ) ? 'pl-big' : 'pr-big';
?>

<header id="<?php echo $id;?>" class="op-section headers innovent-header-1 full-screen">
	<div class="navbar navbar-static-top" <?php echo ($settings['sticky_nav']) ? 'data-spy="affix"' : '';?> data-offset-top="80">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#onepager-nav">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo site_url(); ?>">
				<?php if($contents['logo']) :?>
					<img class="img-responsive innovent-header-1-logo" src="<?php echo $contents['logo']?>" alt="<?php wp_title(); ?>">
				<?php else : ?>
					<?php wp_title(); ?>
				<?php endif; ?>
		      </a>
		    </div>

		    <!-- Menu -->
		    <nav class="collapse navbar-collapse" id="onepager-nav">
				<!-- Multilingual Languages -->
				<?php if($contents['languages']):?>
					<ul class="nav navbar-nav navbar-right">
						<?php foreach ($contents['languages'] as $language): ?>
			            	<li class="language-container">
			            		<a href="<?php echo $language['link']['url'];?>" target="<?php echo $language['link']['target']? '_blank':''?>" title="<?php echo $language['link']['text']? $language['link']['text'] : $language['language'];?>"><span class="language language-<?php echo $language['language'];?>"/></a>
			            	</li>
			            <?php endforeach; ?>
		            </ul>
	        	<?php endif; ?>

				<!-- Menu -->
		    	<?php wp_nav_menu(array(
	                'menu' =>$contents['menu'] ,
	                'menu_class'=>'nav navbar-nav navbar-right',
	                'container' =>false,
	              	'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	              	'walker'            => new wp_bootstrap_navwalker()
	            )) ?>
	            
		    </nav>
		</div>
	</div>
	<div class="container innovent-header-1-container">
		<?php // Image Top
		if(trim($contents['image']) && 'top' == $settings['media_alignment']): ?>
			<div class="row">
				<div class="col-sm-12">
					<img src="<?php echo $contents['image']?>" alt="<?php echo $contents['title']?>" class="op-meida img-responsive wow <?php echo $animation_media ?> img-rounded">
				</div>
			</div>
		<?php endif; ?>

		<div class="row">
			<article class="flex flex-<?php echo $settings['content_alignment']?>">
			<?php // Image Left
			if (trim($contents['image']) && 'left' == $settings['media_alignment']) : ?>
				<div class="col-sm-<?php echo $image_cols;?>">
					<img src="<?php echo $contents['image']?>" alt="<?php echo $contents['title']?>" class="op-meida img-responsive wow <?php echo $animation_media ?> img-rounded">
				</div>
			<?php endif; ?>

			<div class="col-sm-<?php echo $content_cols;?>">
				<div class="<?php echo $padding_class?>">
					<!-- Title -->
					<?php if($contents['title']): ?>
						<h1 class="section-title <?php echo $settings['title_transformation']?> <?php echo $settings['title_size']?> wow <?php echo $animation_content ?>"><?php echo $contents['title']?></h1>
					<?php endif; ?>
					<!-- Description -->
					<?php if($contents['description']): ?>
						<div class="section-desc wow <?php echo $animation_content ?>"><?php echo $contents['description']?></div>
					<?php endif; ?>
					
					<div class="wow <?php echo $animation_content ?> btn-row">
						<?php foreach($contents['buttons'] as $button): ?>
							<?php if( $button['url'] AND $button['title']): ?>
								<a class="btn btn-primary btn-lg wow btn-scroll" href="<?php echo $button['url']?>">
									<?php echo $button['title']; ?>
								</a>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<?php // Image right
			if (trim($contents['image']) && 'right' == $settings['media_alignment']) : ?>
				<div class="col-sm-<?php echo $image_cols;?>">
					<img src="<?php echo $contents['image']?>" alt="<?php echo $contents['title']?>" class="op-meida img-responsive wow <?php echo $animation_media ?> img-rounded">
				</div>
			<?php endif; ?>
			</article>
		</div>
		<?php // Image bottom
		if(trim($contents['image']) && 'bottom' == $settings['media_alignment']): ?>
			<div class="row">
				<div class="col-sm-12">
					<img src="<?php echo $contents['image']?>" alt="<?php echo $contents['title']?>" class="op-meida img-responsive wow <?php echo $animation_media ?> img-rounded">
				</div>
			</div>
		<?php endif; ?>
	</div>
</header>
