jQuery(function() {
	jQuery(".btn-scroll").click(function(event) {
		event.preventDefault();
		var href = jQuery(this).attr('href');
		if (href.startsWith('#')) {
			jQuery(window).scrollTo(jQuery(href).offset().top - 70, 800);
		} else {
			window.location = href;
		}
	});
	jQuery("body").scrollspy({
        target: "#onepager-nav",
        offset: 70,
    });
});