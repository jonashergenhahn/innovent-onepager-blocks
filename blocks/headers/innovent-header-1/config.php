<?php

return array(

  'slug'      => 'innovent-header-1', // Must be unique and singular
  'groups'    => array('headers'), // Blocks group for filter and plural
  'name'      => 'header for multilingual sites',

  // Fields - $contents available on view file to access the option
  'contents' => array(

    array('label'=> 'Navbar Content' , 'type'=> 'divider'),
    array(
      'name'=>'logo',
      'type'=>'image',
      'value'=> 'http://s3.amazonaws.com/quantum-assets/logo-black-mid.png'
    ),
    array('name'=>'menu','type'=>'menu'),

    array('label'=> 'Multilingual' , 'type'=> 'divider'),
    array(
      'name'=>'languages',
      'type'=>'repeater',
      'fields' => array(
        array(
          array(
            'name'     => 'language',
            'label'    => 'language',
            'type'     => 'select',
            'options'  => array(
              ''      => 'None',
              'english' => 'English',
              'german'  => 'German',
              'french'  => 'French',
              'spanish' => 'Spanish',
              'suomi'   => 'Suomi',
            ),
          ),
          array('name'=>'link', 'type' => 'link'),
        ),
      )
    ),

    array('label'=> 'Header Content' , 'type'=> 'divider'),
    array(
      'name'=>'title',
      'value' => 'Lets make a better website together'
    ),
    array(
      'name'=>'description',
      'type'=>'editor',
      'value'=> 'The world is a dangerous place to live; not because of the people who are evil, but because of the people who dont do anything about it.'
    ),
    array('type' => 'divider', 'label' => 'Buttons'),
    array(
      'name' => 'buttons',
      'type' => 'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'type' => 'text', 'label' => 'Title'),
          array('name'=>'url', 'type' => 'text', 'url'),
        )
      )
    ),
    array(
      'name'=>'image',
      'type'=>'image',
      'value'=> 'http://s3.amazonaws.com/quantum-assets/phone-dark.png'
    ),

  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(

    // Navbar 
    array( 'lable' => 'Navbvar Settings', 'type' => 'divider'),
    array(
      'name' => 'sticky_nav',
      'label' => 'Sticky Nav',
      'type' => 'select',
      'value' => 1,
      'options' => array(
        1 => 'Enabled',
        0 => 'Disabled'
      )
    ),
    array(
      'name' => 'nav_collapse',
      'label' => 'Nav Collapse Size',
      'append' => 'px',
      'value' => '768'
    ),

    // Section Settings
    array('label' => 'Section', 'type' => 'divider'),
    array(
      'name' => 'section_height',
      'label' => 'Section Height',
      'append' => 'px',
      'prepend' => 'auto or px',
      'value' => 'auto'
    ),
    
    // Media Settings
    array('label' => 'Media Settings', 'type' => 'divider'),
    array(
      'name'     => 'media_alignment',
      'label'    => 'Meida Alignment',
      'type'     => 'select',
      'value'    => 'right',
      'options'  => array(
        'left'    => 'Left',
        'right'   => 'Right',
        'top'     => 'Top',
        'bottom'     => 'Bottom',
      ),
    ),
    array(
      'name'     => 'media_grid',
      'label'    => 'Media Grid',
      'type'     => 'select',
      'value'    => '4',
      'options'  => array(
        '3'   => '3',
        '4'   => '4',
        '5'   => '5',
        '6'   => '6',
        '7'   => '7',
        '8'   => '8'
      ),
    ),

    // Text settings
    array('label' => 'Text Settings', 'type' => 'divider'),
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'text_size',
      'label' => 'Text Size',
      'append' => 'rem',
      'prepend' => 'relative',
      'value' => '1'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-uppercase',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      ),
    ),
    array(
      'name'     => 'content_alignment',
      'label'    => 'Items Alignment',
      'type'     => 'select',
      'value'    => 'middle',
      'options'  => array(
        'top'      => 'Top',
        'middle'   => 'Middle',
        'bottom'   => 'Bottom'
      ),
    ),

    // Animation 
    array('type' => 'divider', 'label' => 'Animation'),
    array(
      'name'     => 'animation_content',
      'label'    => 'Animation Content',
      'type'     => 'select',
      'value'    => 'fadeInUp',
      'options'  => array(
        '0'           => 'None',
        'fadeIn'      => 'Fade',
        'fadeInLeft'  => 'Slide Left',
        'fadeInRight' => 'Slide Right',
        'fadeInUp'    => 'Slide Up',
        'fadeInDown'  => 'Slide Down',
      ),
    ),

   array(
    'name'     => 'animation_media',
    'label'    => 'Animation Media',
    'type'     => 'select',
    'value'    => 'fadeInRight',
    'options'  => array(
        '0'             => 'None',
        'fadeIn'        => 'Fade',
        'fadeInLeft'    => 'Slide Left',
        'fadeInRight'   => 'Slide Right',
        'fadeInUp'      => 'Slide Up',
        'fadeInDown'    => 'Slide Down',
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('label'=> 'Nav Style' , 'type'=> 'divider'),
    array(
      'name'  => 'nav_bg_static',
      'label' => 'Static Background',
      'type'  => 'colorpicker',
      'value' => 'rgba(0,0,0,0.5)'
    ),
    array(
      'name'  => 'nav_bg',
      'label' => 'Sticky Background',
      'type'  => 'colorpicker',
      'value' => 'rgba(0,0,0,0.5)'
    ),
    array(
      'name'  => 'link_color',
      'label' => 'Link Color',
      'type'  => 'colorpicker',
      'value' => '#fff'
    ),
    array(
      'name'  => 'link_hover_color',
      'label' => 'Link Hover Color',
      'type'  => 'colorpicker',
      'value' => '@color.primary'
    ),
    array('label'=>'Background', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image',
      'value' => 'http://s3.amazonaws.com/quantum-assets/bg/bg5.jpg'
    ),
    array(
      'name'     => 'bg_repeat',
      'label'    => 'Repeat',
      'type'     => 'select',
      'value'    => 'no-repeat',
      'options'  => array(
        'no-repeat'     => 'No Repeat',
        'repeat-x'      => 'Repeat X',
        'repeat-y'      => 'Repeat Y',
      )
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Color',
      'type'    => 'colorpicker',
      'value'   => '#ebeff2'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background'
    ),
    array('label'=>'Text', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'  => 'text_color',
      'label' => 'Text Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'    => 'button_border_color',
      'label'   => 'Button Border',
      'type'    => 'colorpicker',
      'value'   => '#323232'
    ),
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Text',
      'type'    => 'colorpicker',
      'value'   => '#323232'
    ),
  ),

  'assets' => function( $path ){
    Onepager::addStyle( 'innovent-header-1', $path . '/style.css' );
    Onepager::addScript( 'innovent-header-1', $path .'/js/functions.js');
  }
);
