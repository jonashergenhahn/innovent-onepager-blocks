<?php

return array(

  'slug'      => 'innovent-parallax-bg-1', // Must be unique
  'groups'    => array('contents'), // Blocks group for filter
  'name'      => 'Parallax background section',

  // Fields - $contents available on view file to access the option
  'contents' => array(
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'height',
      'label' => 'Section Height',
      'append' => 'px',
      'value' => '160'
    )
  ),

  'styles' => array(
    array(
      'name'  => 'bg_image',
      'label' => 'Background Image',
      'type'  => 'image'
    ),
    array(
      'name'     => 'bg_repeat',
      'label'    => 'Image Repeat',
      'type'     => 'select',
      'value'    => 'no-repeat',
      'options'  => array(
        'no-repeat'     => 'No Repeat',
        'repeat-x'      => 'Repeat X',
        'repeat-y'      => 'Repeat Y',
      )
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Background Color',
      'type'    => 'colorpicker',
      'value'   => '#f5f5f5'
    ),
    array(
      'name'=>'bg_parallax',
      'type'=> 'switch',
      'label'=>'Parallax Background',
      'value' => true
    )
  )
);
