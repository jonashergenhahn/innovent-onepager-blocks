<?php

return array(

  'slug'      => 'innovent-team-2', // Must be unique
  'groups'    => array('teams'), // Blocks group for filter
  'name'      => 'team with description text',

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('name'=>'title', 'value'=> 'About the company'),

    array(
      'name'=>'members',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name'=>'title', 'label' => 'Name', 'value' => 'Steve Jobs'),
          array('name'=>'designation', 'value' => 'CEO, Apple Inc'),
          array('name'=>'image','type'=>'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/4-thumb.jpg'),
          array('name'=>'social', 'label' => 'Social Profiles', 'value' => array('https://facebook.com/innoventsalo', 'https://instagram.com/innoventsalo', 'https://twitter.com/innoventsalo')),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),

          array('name'=>'quote', 'type' => 'textarea', 'label' => 'Quote', 'value' => 'Change is the law of life. And those who look only to the past or present are certain to miss the future.'),
          array('name'=>'citation', 'label' => 'Quote author', 'value' => 'John F. Kennedy'),
          array('name'=>'description', 'type' => 'editor'),
        ),
        array(
          array('name'=>'title', 'label' => 'Name', 'value' => 'Nikola Tesla'),
          array('name'=>'designation', 'value' => 'Scientist'),
          array('name'=>'image','type'=>'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/5-thumb.jpg'),
          array('name'=>'social', 'label' => 'Social Profiles', 'value' => array('https://twitter.com/innoventsalo')),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),

          array('name'=>'quote', 'type' => 'textarea', 'label' => 'Quote',),
          array('name'=>'citation', 'label' => 'Quote author',),
          array('name'=>'description', 'type' => 'editor', 'value' => 'Nikola Tesla (Serbian Cyrillic: Никола Тесла; 10 July 1856 – 7 January 1943) was a Serbian-American inventor, electrical engineer, mechanical engineer, physicist, and futurist who is best known for his contributions to the design of the modern alternating current (AC) electricity supply system. Tesla gained experience in telephony and electrical engineering before emigrating to the United States in 1884 to work for Thomas Edison in New York City. He soon struck out on his own with financial backers, setting up laboratories and companies to develop a range of electrical devices. His patented AC induction motor and transformer were licensed by George Westinghouse, who also hired Tesla for a short time as a consultant. His work in the formative years of electric-power development was involved in a corporate alternating current/direct current "War of Currents" as well as various patent battles. He became a naturalized US citizen in 1891.'),
        ),
        array(
          array('name'=>'title', 'label' => 'Name', 'value' => 'Elon Musk'),
          array('name'=>'designation', 'value' => 'CEO, Tesla Motors'),
          array('name'=>'image','type'=>'image', 'value' => 'http://s3.amazonaws.com/quantum-assets/images/6-thumb.jpg'),
          array('name'=>'social', 'label' => 'Social Profiles', 'value' => array('https://instagram.com/innoventsalo', 'https://facebook.com/innoventsalo')),
          array('name'=>'link', 'placeholder'=> home_url()),
          array('name'=>'target', 'label'=>'open in new window', 'type'=>'switch'),

          array('name'=>'quote', 'type' => 'textarea', 'label' => 'Quote', 'value' => 'Darkness cannot drive out darkness; only light can do that. Hate cannot drive out hate; only love can do that.'),
          array('name'=>'citation', 'label' => 'Quote author', 'value' => 'Martin Luther King, Jr.'),
          array('name'=>'description', 'type' => 'editor'),
        ),
      )
    )
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name' => 'font_size',
      'label' => 'Font Size',
      'append' => 'rem',
      'prepend' => 'relative',
      'value' => '1',
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-uppercase',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      )
    ),
    array(
      'name'     => 'animation',
      'label'    => 'Animation Title',
      'type'     => 'select',
      'value'    => 'fadeInLeft',
      'options'  => array(
        '0'           => 'None',
        'fadeIn'      => 'Fade',
        'zoomIn'        => 'Zoom In',
        'fadeInLeft'  => 'Slide Left',
        'fadeInRight' => 'Slide Right',
        'fadeInUp'    => 'Slide Up',
        'fadeInDown'  => 'Slide Down',
      )
    ),
    array(
      'name' => 'media_size',
      'label' => 'Media Size',
      'type' => 'select',
      'value' => 4,
      'options' => array(
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5'
      )
    ),
    array(
      'name'     => 'overlay_animation',
      'label'    => 'Overlay Animation',
      'type'     => 'select',
      'value'    => 'scale',
      'options'  => array(
        'slide-top'     => 'Slide Top',
        'slide-bottom'  => 'Slide Bottom',
        'slide-left'    => 'Slide Left',
        'slide-right'   => 'Slide Right',
        'fade'          => 'Fade',
        'scale'         => 'Scale',
        'spin'          => 'Spin',
      )
    )
  ),

  'styles' => array(
    array(
      'name'=>'bg_color',
      'label' => 'Background Color',
      'type'  => 'color',
      'value' => '#fff'
    ),
    array(
      'name'=>'row_even_bg_color',
      'label' => 'Row(even) Background Color',
      'type'  => 'color',
      'value' => ''
    ),
    array(
      'name'=>'row_odd_bg_color',
      'label' => 'Row(odd) Background Color',
      'type'  => 'color',
      'value' => ''
    ),
    array(
      'name'=>'title_color',
      'label' => 'Title Color',
      'type'  => 'color',
      'value' => '#323232'
    )
  ),

  'assets' => function( $path ){
      Onepager::addStyle('innovent-team-2', $path . '/style.css');
  }
);
