<?php
	$animation = ($settings['animation']) ? $settings['animation'] : '';
	$animation_inverted = '';
	switch($animation) {
		case 'fadeInLeft':
			$animation_inverted = 'fadeInRight';
			break;
		case 'fadeInRight':
			$animation_inverted = 'fadeInLeft';
			break;
		default:
			$animation_inverted = $animation;
	}

	$media_size = $settings['media_size'] ? $settings['media_size'] : 4;
	$content_size = 12 - $media_size;
?>
<section id="<?php echo $id; ?>" class="op-section teams innovent-team-2 full-screen">
	<div class="container">
		<?php if($contents['title']): ?>
			<h1 class="section-title ml-big mr-big text-center <?php echo $settings['title_transformation']?>">
				<?php echo $contents['title']?>
			</h1>
		<?php endif;?>

		<?php foreach( $contents['members'] as $key => $member):?>
			<div class="row wow <?php echo ($key%2==1)? $animation_inverted : $animation;?>">
				<div class="col-sm-<?php echo $media_size;?> <?php echo ($key%2==1)? 'col-sm-push-'.$content_size :'';?> text-center member-image">
					<figure class="overlay overlay-hover">
						<img class="img-responsive" src="<?php echo $member['image']?>" alt="<?php echo $member['title']?>" />
						<figcaption class="overlay-panel overlay-background overlay-<?php echo $settings['overlay_animation']?> flex flex-center flex-middle text-center">
							<div>
								<h3 class="title">
				                  <?php if(trim($member['link'])): ?>
				                    <a href="<?php echo $member['link'] ?>" target="<?php echo $member['target'] ? '_blank' : ''?>"><?php echo $team['title']?></a>
				                  <?php else: ?>
				                    <?php echo $member['title']?>
				                  <?php endif; ?>
				                </h3>
								<p class="designation"><?php echo $member['designation']?></p>
								<div class="social-links">
								<?php foreach($member['social'] as $social):?>
									<a class="icon" href="<?php echo $social?>" target="_blank"></a>
								<?php endforeach;?>
								</div>
							</div>
						</figcaption>
					</figure>
				</div>
				<div class="col-sm-<?php echo $content_size;?> <?php echo ($key%2==1)? 'col-sm-pull-'.$media_size :'';?> member-description">
					<?php if(trim($member['quote'])):?>
						<blockquote>
							<p><?php echo $member['quote'];?></p>
							<footer><?php echo (trim($member['citation']))? $member['citation'] : 'Unkown';?></footer>
						</blockquote>
					<?php endif; ?>
					<div>
						<?php echo $member['description'];?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
