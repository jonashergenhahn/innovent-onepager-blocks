#<?php echo $id; ?>{
	background : <?php echo $styles['bg_color'];?>;
	font-size: <?php echo $settings['font_size'];?>rem;
}
#<?php echo $id; ?> .section-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color'];?>;
}
@media screen and (min-width: 768px) {
	#<?php echo $id; ?> .row:nth-child(even) {
		background-color: <?php echo $styles['row_even_bg_color'];?>;
	}
	#<?php echo $id; ?> .row:nth-child(odd) {
		background-color: <?php echo $styles['row_odd_bg_color'];?>;
	}
}
