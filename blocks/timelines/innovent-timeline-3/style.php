#<?php echo $id ?>{
	background: url(<?php echo $styles['bg_image'] ?>);
	background-color: <?php echo $styles['bg_color'] ?>;
	<?php if($styles['bg_parallax']):?>
	background-attachment : fixed;
	<?php endif;?>
	background-size : cover;
	color : <?php echo $styles['text_color']?>;
}
#<?php echo $id ?> .section-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
}
.innovent-timeline-3 .timeline:before {
	background-color: <?php echo $styles['timeline_color'];?>;
	box-shadow: 0 0 4px <?php echo $styles['timeline_color'];?>;
}
.innovent-timeline-3 .timeline .timeline-item .timeline-panel {
	background-color: <?php echo $styles['panel_bg_color'];?>;
	border-color: <?php echo $styles['panel_border_color'];?>;
	box-shadow: 0 0 4px <?php echo $styles['panel_border_color'];?>;
}
.innovent-timeline-3 .timeline-horizontal .timeline-item .timeline-panel:before {
	border-top-color: <?php echo $styles['panel_border_color'];?> !important;
	border-bottom-color: <?php echo $styles['panel_border_color'];?> !important;
}
.innovent-timeline-3 .timeline .timeline-item .timeline-panel .timeline-title {
	color: <?php echo $styles['panel_heading_color'];?>;
}
.innovent-timeline-3 .timeline-horizontal .timeline-panel .timeline-heading .timeline-subheading {
	color: <?php echo $styles['panel_subheading_color'];?> !important;
}
.innovent-timeline-3 .timeline-horizontal .timeline-panel .timeline-body {
	color: <?php echo $styles['panel_text_color'];?> !important;
}