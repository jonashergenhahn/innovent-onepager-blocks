<?php
	$animation = ($settings['animation']) ? $settings['animation'] : '';
?>
<section id="<?php echo $id;?>" class="op-section contents full-screen innovent-timeline-3">
	<div class="container">
		<h1 class="section-title">
			<?php echo $contents['title']; ?>
		</h1>
		<div style="display:inline-block;width:100%;overflow-y:auto;">
			<ul class="timeline timeline-horizontal">
				<?php foreach($contents['timelines'] as $timeline): ?>
				<li class="timeline-item wow <?php echo $animation; ?>">
					<?php if(startsWith($timeline['badge'], 'fa ')): ?>
						<div class="timeline-badge primary <?php echo $timeline['badge_color'];?>"><i class="<?php echo $timeline['badge'];?>"></i></div>
					<?php else: ?>
						<img class="timeline-badge timeline-badge-image <?php echo $timeline['badge_color'];?>" src="<?php echo $timeline['badge']?>"/>
					<?php endif; ?>

					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title"><?php echo $timeline['title'];?></h4>
							<p><small class="text-muted timeline-subheading"><?php echo $timeline['subheading'];?></small></p>
						</div>
						<div class="timeline-body">
							<?php echo $timeline['text'];?>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</section>