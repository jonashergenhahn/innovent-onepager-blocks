<?php

return array(

  'slug'      => 'innovent-timeline-3', // Must be unique and singular
  'groups'    => array('contents', 'timelines'), // Blocks group for filter and plural
  'name'      => 'horizontal timeline',

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('name' => 'title', 'value' => 'Timeline'),
    array('name' => 'text', 'type' => 'textarea', 'value' => 'Some text'),
    array(
      'name' => 'timelines',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name' => 'title', 'label' => 'Heading', 'value' => 'Microsoft'),
          array('name' => 'subheading', 'label' => 'Subtitle', 'value' => '1998 - 2010'),
          array('name' => 'text', 'type' => 'editor', 'label' => 'Content', 'value' => 'You need some text.'),
          array('name' => 'badge', 'type' => 'media'),
          array(
            'name' => 'badge_color',
            'label' => 'Badge color',
            'type' => 'select',
            'value' => 'primary',
            'options' => array(
              ''        => 'none',
              'warning' => 'yellow',
              'danger'  => 'red',
              'primary' => 'blue',
              'success' => 'green',
              'info'    => 'light blue',
            )
          ),
        ),
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-uppercase',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => 'fadeInUp',
      'options'  => array(
        '0'           => 'None',
        'fadeIn'      => 'Fade',
        'fadeInLeft'  => 'Slide Left',
        'fadeInRight' => 'Slide Right',
        'fadeInUp'    => 'Slide Up',
        'fadeInDown'  => 'Slide Down',
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('type' => 'devider', 'label' => 'Background'),
    array('name' => 'bg_color', 'type' => 'color', 'label' => 'Background Color'),
    array('name' => 'bg_image', 'type' => 'image', 'label' => 'Background Image'),
    array('name' => 'bg_parallax', 'type' => 'switch', 'label' =>'Parallax Background'),

    array('type' => 'devider', 'label' => 'Colors'),
    array('name' => 'timeline_color', 'type' => 'color', 'label' => 'Timeline Color', 'value' => '#eeeeee'),
    array('name' => 'title_color', 'type' => 'color', 'label' => 'Title Color'),
    array('name' => 'panel_bg_color', 'type' => 'color', 'label' => 'Panel Background Color', 'value' => '#ffffff'),
    array('name' => 'panel_border_color', 'type' => 'color', 'label' => 'Panel Border Color', 'value' => '#c0c0c0'),
    array('name' => 'panel_heading_color', 'type' => 'color', 'label' => 'Panel Heading Color', 'value' => '#555555'),
    array('name' => 'panel_subheading_color', 'type' => 'color', 'label' => 'Panel Subtitle Color', 'value' => '#999999'),
    array('name' => 'panel_text_color', 'type' => 'color', 'label' => 'Panel Text Color', 'value' => '#000000'),
  ),

  'assets' => function( $path ){
    Onepager::addStyle('innovent-timeline-3', $path . '/style.css');
  }
);
