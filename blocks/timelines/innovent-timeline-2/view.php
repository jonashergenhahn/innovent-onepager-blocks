<?php
	$animation = ($settings['animation']) ? $settings['animation'] : '';
?>
<section id="<?php echo $id;?>" class="op-section contents innovent-timeline-2 full-screen">
	<div class="container">
	  <div class="section-title">
		<h1><?php echo $contents['title']; ?></h1>
	  </div>
	  <div class="row">
	    <div class="col-lg-12">
	      <p>
	        <?php echo $contents['text']; ?>
	      </p>
	      <ul class="timeline">
	      	<?php
	      		for($i=0; $i < count($contents['timelines']); $i++):
	      			$entry = $contents['timelines'][$i]; ?>
			        <li class="<?php echo $i%2==1? 'timeline-inverted' : ''; ?>">
			          <img class="timeline-image img-circle img-responsive" src="<?php echo $entry['image'];?>" alt="">
			          <div class="timeline-panel">
			            <div class="timeline-heading">
			              <h4><?php echo $entry['title'];?></h4>
			              <h4 class="subheading"><?php echo $entry['subheading'];?></h4>
			            </div>
			            <div class="timeline-body">
			              <p class="text-muted">
			                <?php echo $entry['text'];?>
			              </p>
			            </div>
			          </div>
			          <?php if($i < (count($contents['timelines'])-1)): ?>
			          	<div class="line"></div>
			          <?php endif; ?>
			        </li>
			<?php endfor; ?>
	      </ul>
	    </div>
	  </div>
	</div>
</section>