<?php

return array(

  'slug'      => 'innovent-timeline-2', // Must be unique and singular
  'groups'    => array('contents', 'timelines'), // Blocks group for filter and plural
  'name'      => 'responsive zigzag timeline',

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('name' => 'title', 'value' => 'Timeline'),
    array('name' => 'text', 'type' => 'textarea', 'value' => 'Some text'),
    array(
      'name' => 'timelines',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name' => 'title', 'label' => 'Heading', 'value' => 'Microsoft'),
          array('name' => 'subheading', 'label' => 'Subtitle', 'value' => '1998 - 2010'),
          array('name' => 'text', 'type' => 'editor', 'label' => 'Content', 'value' => 'You need some text.'),
          array('name' => 'image', 'type' => 'image'),
        ),
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-uppercase',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => 'fadeInUp',
      'options'  => array(
        '0'           => 'None',
        'fadeIn'      => 'Fade',
        'fadeInLeft'  => 'Slide Left',
        'fadeInRight' => 'Slide Right',
        'fadeInUp'    => 'Slide Up',
        'fadeInDown'  => 'Slide Down',
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('type' => 'devider', 'label' => 'Background'),
    array('name' => 'bg_color', 'type' => 'color', 'label' => 'Background Color'),
    array('name' => 'bg_image', 'type' => 'image', 'label' => 'Background Image'),
    array('name' => 'bg_parallax', 'type' => 'switch', 'label' =>'Parallax Background'),

    array('type' => 'devider', 'label' => 'Colors'),
    array('name' => 'image_bg_color', 'type' => 'color', 'label' => 'Image Background'),
    array('name' => 'image_border_color', 'type' => 'color', 'label' => 'Image Border'),
    array('name' => 'line_color', 'type' => 'color', 'label' => 'Lines'),

    array('type' => 'devider', 'label' => 'Text Color'),
    array('name' => 'title_color', 'type' => 'color', 'label' => 'Title Color'),
    array('name' => 'timeline_heading_color', 'type' => 'color', 'label' => 'Heading Color'),
    array('name' => 'timeline_subheading_color', 'type' => 'color', 'label' => 'Subheading Color'),
    array('name' => 'timeline_body_color', 'type' => 'color', 'label' => 'Body Color'),
  ),

  'assets' => function( $path ){
    Onepager::addStyle('innovent-timeline-2', $path . '/style.css');
  }
);
