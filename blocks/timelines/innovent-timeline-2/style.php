#<?php echo $id ?>{
	background: url(<?php echo $styles['bg_image'] ?>);
	background-color: <?php echo $styles['bg_color'] ?>;
	<?php if($styles['bg_parallax']):?>
	background-attachment : fixed;
	<?php endif;?>
	background-size : cover;
	color : <?php echo $styles['text_color']?>;
}
#<?php echo $id ?> .section-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
}
#<?php echo $id; ?> .timeline>li .timeline-image {
	border-color: <?php echo $styles['image_border_color']; ?>;
	box-shadow: 0 0 5px <?php echo $styles['image_border_color']; ?>;
	background-color: <?php echo $styles['image_bg_color']; ?>;
}
#<?php echo $id; ?> .timeline>li:nth-child(odd) .line:before, #<?php echo $id; ?> .timeline>li:nth-child(even) .line:before {
	background-color: <?php echo $styles['line_color']; ?>;
	box-shadow: 0 0 5px <?php echo $styles['line_color']; ?>;
}

.innovent-timeline-2 .timeline .timeline-heading h4 {
	color: <?php echo $styles['timeline_heading_color'];?>;
}

.innovent-timeline-2 .timeline .timeline-heading h4.subheading {
	color: <?php echo $styles['timeline_subheading_color'];?>;	
}

.innovent-timeline-2 .timeline .timeline-body>p,
.innovent-timeline-2 .timeline .timeline-body>ul {
	color: <?php echo $styles['timeline_body_color'];?>;	
}