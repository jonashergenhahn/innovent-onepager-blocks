#<?php echo $id ?>{
	background: url(<?php echo $styles['bg_image'] ?>);
	background-color: <?php echo $styles['bg_color'] ?>;
	<?php if($styles['bg_parallax']):?>
	background-attachment : fixed;
	<?php endif;?>
	background-size : cover;
	color : <?php echo $styles['text_color']?>;
}
#<?php echo $id ?> .section-title{
	font-size : <?php echo $settings['title_size']?>px;
	color : <?php echo $styles['title_color']?>;
}
#<?php echo $id ?> .btn{
	background: transparent;
	border: 3px solid <?php echo $styles['button_border_color']?>;
	color : <?php echo $styles['button_text_color']?>;
}

#<?php echo $id ?> .btn:hover{
	background: <?php echo $styles['button_border_color']?>;
}
#<?php echo $id ?> .timeline > li > .timeline-panel, #<?php echo $id ?> .timeline:before {
	background: <?php echo $styles['bg_bubble']?>;
}
#<?php echo $id ?> .timeline > li > .timeline-panel {
	border: none;
}
.timeline > li > .timeline-panel:after {
	border-left-color: <?php echo $styles['bg_bubble']?>;
	border-right-color: <?php echo $styles['bg_bubble']?>;
}
.timeline > li > .timeline-panel::before {
	border-left-color: transparent;
	border-right-color: transparent;
}
#<?php echo $id ?> .timeline-title {
	color: <?php echo $styles['bubble_heading_color']?>;
}
#<?php echo $id ?> .text-muted {
	color: <?php echo $styles['bubble_subtitle_color']?>;
}
#<?php echo $id ?> .timeline-body {
	color: <?php echo $styles['bubble_color']?>;
}