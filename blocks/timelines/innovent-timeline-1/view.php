<?php
	$animation = ($settings['animation']) ? $settings['animation'] : '';
?>
<section id="<?php echo $id;?>" class="op-section contents innovent-timeline-1 full-screen">
	<div class="container">
	    <div class="section-title">
	        <h1><?php echo $contents['title']; ?></h1>
	    </div>
	    <ul class="timeline">
	    	<?php foreach($contents['timelines'] as $timeline): ?>
		        <li class="<?php echo $timeline['invert']? 'timeline-inverted' : '';?>">
		          <?php if($timeline['badge_color']): ?>
		          	<div class="timeline-badge <?php echo $timeline['badge_color']? $timeline['badge_color'] : '';?>"><i class="<?php echo $timeline['icon'];?>"></i></div>
		          <?php endif; ?>
		          <div class="timeline-panel">
		            <div class="timeline-heading">
		              <h4 class="timeline-title"><?php echo $timeline['title']; ?></h4>
		              <p><small class="text-muted"><?php echo $timeline['subtitle']; ?></small></p>
		            </div>
		            <div class="timeline-body">
		              <p><?php echo $timeline['body'];?></p>
		            </div>
		          </div>
		        </li>
	    	<?php endforeach; ?>
	    </ul>
	</div>
</section>
