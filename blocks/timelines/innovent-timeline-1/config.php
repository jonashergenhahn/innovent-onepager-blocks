<?php

return array(

  'slug'      => 'innovent-timeline-1', // Must be unique and singular
  'groups'    => array('contents', 'timelines'), // Blocks group for filter and plural
  'name'      => 'responsive vertical timeline',

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('name' => 'title', 'value' => 'Timeline'),
    array(
      'name' => 'timelines',
      'type'=>'repeater',
      'fields' => array(
        array(
          array('name' => 'title', 'label' => 'Heading', 'value' => 'Microsoft'),
          array('name' => 'subtitle', 'label' => 'Subtitle', 'value' => '1998 - 2010'),
          array('name' => 'body', 'type' => 'editor', 'label' => 'Content', 'value' => 'You need some text.'),
          array(
            'name' => 'badge_color',
            'label' => 'Badge color',
            'type' => 'select',
            'value' => 'default',
            'options' => array(
              ''        => 'none',
              'default' => 'default',
              'warning' => 'yellow',
              'danger'  => 'red',
              'primary' => 'blue',
              'success' => 'green',
              'info'    => 'light blue',
            )
          ),
          array('name' => 'icon', 'type' => 'icon'),
          array('name'=>'invert','type'=> 'switch','label'=>'Invert bubble'),
        ),
      )
    ),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(
    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '@section_title_size'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-uppercase',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      ),
    ),

    array(
      'name'     => 'animation',
      'label'    => 'Animation',
      'type'     => 'select',
      'value'    => 'fadeInUp',
      'options'  => array(
        '0'           => 'None',
        'fadeIn'      => 'Fade',
        'fadeInLeft'  => 'Slide Left',
        'fadeInRight' => 'Slide Right',
        'fadeInUp'    => 'Slide Up',
        'fadeInDown'  => 'Slide Down',
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('type' => 'devider', 'label' => 'Background'),
    array('name' => 'bg_color', 'type' => 'color', 'label' => 'Background Color'),
    array('name' => 'bg_image', 'type' => 'image', 'label' => 'Background Image'),
    array('name' => 'bg_parallax', 'type' => 'switch', 'label' =>'Parallax Background'),

    array('type' => 'devider', 'label' => 'Colors'),
    array('name' => 'title_color', 'type' => 'color', 'label' => 'Title Color'),
    array('name' => 'bg_bubble', 'type' => 'color', 'label' => 'Bubble Background Color'),
    array('name' => 'bubble_heading_color', 'type' => 'color', 'label' => 'Bubble Heading Color'),
    array('name' => 'bubble_subtitle_color', 'type' => 'color', 'label' => 'Bubble Subtitle Color'),
    array('name' => 'bubble_color', 'type' => 'color', 'label' => 'Bubble Text Color'),
  ),

  'assets' => function( $path ){
    Onepager::addStyle('innovent-timeline-1', $path . '/style.css');
  }
);
