<?php
	$animation_form = ($settings['animation_form']) ? $settings['animation_form'] : '';
?>
<section id="<?php echo $id;?>" class="op-section contacts innovent-contact-1 full-screen">
	<div class="container">
		<div class="row wow <?php echo $animation_form;?>">
				<div class="col-md-offset-3 col-md-6">
					<h2 class="section-title <?php echo $settings['title_transformation'];?>"><?php echo $contents['title']; ?></h2>
					<div class="mr-big">
						<?php echo do_shortcode($contents['form']);?>
					</div>
				</div>
		</div>
	</div>
</section>
