<?php

return array(

  'slug'      => 'innovent-contact-1', // Must be unique and singular
  'groups'    => array('contact'), // Blocks group for filter and plural
  'name'      => 'contact form',

  // Fields - $contents available on view file to access the option
  'contents' => array(
    array('name' =>'title', 'value' => 'contact us'),
    array('name' => 'form', 'label' => 'Contact Form Shortcode', 'type' => 'textarea'),
  ),

  // Settings - $settings available on view file to access the option
  'settings' => array(

    array(
      'name' => 'title_size',
      'label' => 'Title Size',
      'append' => 'px',
      'value' => '22'
    ),
    array(
      'name'     => 'title_transformation',
      'label'    => 'Title Transformation',
      'type'     => 'select',
      'value'    => 'text-capitalize',
      'options'  => array(
        'text-lowercase'   => 'Lowercase',
        'text-uppercase'   => 'Uppercase',
        'text-capitalize'  => 'Capitalized'
      ),
    ),

   array(
    'name'     => 'animation_form',
    'label'    => 'Animation Form',
    'type'     => 'select',
    'value'    => 'fadeInRight',
    'options'  => array(
        '0'             => 'None',
        'fadeIn'        => 'Fade',
        'fadeInLeft'    => 'Slide Left',
        'fadeInRight'   => 'Slide Right',
        'fadeInUp'      => 'Slide Up',
        'fadeInDown'    => 'Slide Down',
      ),
    ),
  ),

  // Fields - $styles available on view file to access the option
  'styles' => array(
    array('label'=>'Background', 'type'=>'divider'), // Divider - Background
    array(
      'name'  => 'bg_image',
      'label' => 'Image',
      'type'  => 'image'
    ),
    array(
      'name'     => 'bg_repeat',
      'label'    => 'Repeat',
      'type'     => 'select',
      'options'  => array(
        'no-repeat'     => 'No Repeat',
        'repeat-x'      => 'Repeat X',
        'repeat-y'      => 'Repeat Y',
      )
    ),
    array(
      'name'    => 'bg_color',
      'label'   => 'Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
    array('label'=>'Text', 'type'=>'divider'), // Divider - Text
    array(
      'name'  => 'title_color',
      'label' => 'Title Color',
      'type'  => 'colorpicker',
      'value' => '#222'
    ),
    array(
      'name'  => 'text_color',
      'label' => 'Text Color',
      'type'  => 'colorpicker',
      'value' => '#323232'
    ),
    array(
      'name'    => 'accent_color',
      'label'   => 'Accent Color',
      'type'    => 'colorpicker',
      'value'   => '@color.accent'
    ),
    array(
      'name'    => 'button_text_color',
      'label'   => 'Button Text Color',
      'type'    => 'colorpicker',
      'value'   => '#fff'
    ),
  ),

  'assets' => function( $path ){
    Onepager::addStyle('innovent-contact-1', $path . '/style.css');
  }
);
