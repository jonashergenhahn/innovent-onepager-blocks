# innovent-onepager-blocjs
## Onepager - Innovent Blocks Plugin

Contributors: jonashergenhahn

Tags: page builder, builder, onepage builder, drag and drop, reactjs, bootstrap, fontAwesome, gulp, less, layout, grid, composer, webpack, bower, ui

Stable tag: 1.2.7

Requires at least: 4.2

Tested up to: 4.7

License: GPLv3 or later

License URI: http://www.gnu.org/licenses/gpl-3.0.html

Modern, Powerful & Crazy Fast one page builder. Built with modern tools such ReactJS for next generation theming.

## Description

Onepager Innovent Blocks Plugin adds new blocks to the onepager. Currently you find blocks for different timelines (vertical and horizontal), multilingual teams and multilingual sites.

**Requirements**

 * PHP 5.5+

**Features**

* One header block
* Two team blocks
* Three timeline blocks

## Installation

Installing the plugins is just like installing other WordPress plugins.


## Changelog
### 1.1.1 (2017-02-03)
- Group timelines added
- New Plugin description

### 1.1.0 (2017-01-30)
- Adding block team with description text

### 1.0.1 (2017-01-30)
- display problems horizontal timeline
- display problems with responsive zigzag timeline

### 1.0.0 (2017-01-28)
Plugin release with the first blocks.
