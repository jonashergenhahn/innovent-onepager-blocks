<?php
/**
 * Plugin Name:       Onepager - Innovent Blocks Plugin
 * Plugin URI:        http://innovent-finland.com/onepager-plugin/#onepager-blocks
 * Description:       Onepager Innovent Blocks Plugin adds new blocks to the onepager. Currently you find blocks for different timelines (vertical and horizontal), multilingual teams and multilingual sites.
 * Version:           1.2.7
 * Author:            Innovent Finland
 * Author URI:        http://www.innovent-finland.com
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       onepager-innovent-blocks
 * Domain Path:       /languages
 * Bitbucket Plugin URI: jonashergenhahn/innovent-onepager-blocks
 * Bitbucket Branch:  master
 */

add_action('onepager_loaded', function(){

	/**
	*	Load Blocks
	**/
	$blocks_path = __DIR__ . "/blocks";
	$blocks_url = plugins_url('blocks', __FILE__);
	$blocks_groups = ['Innovent Blocks (Plugin)'];

	onepager()->blockManager()->loadAllFromPath($blocks_path, $blocks_url, $blocks_groups);

});

add_action( 'wp_enqueue_scripts', 'innovent_onepager_blocks_scripts_enqueue' );

function innovent_onepager_blocks_scripts_enqueue() {
    wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), NULL, true );
    wp_register_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, NULL, 'all' );
 
    wp_enqueue_script( 'bootstrap' );
    wp_enqueue_style( 'bootstrap' );
}
